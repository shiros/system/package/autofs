<div align="center">

# System - AutoFS

AutoFS configuration files.

</div>

[[_TOC_]]

## ℹ️ About the project

This project contains the AutoFS configuration files.  
To access the configurations corresponding to your OS, please select the branch corresponding to your OS.

## 👨🏻‍ Authors

This project was created by **Alexandre Caillot (Shiroe_sama)**.

- **[Alexandre Caillot (Shiroe_sama)](https://gitlab.com/Shiroe_sama)**

## 📖 License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT).
